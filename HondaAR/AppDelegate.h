//
//  AppDelegate.h
//  HondaAR
//
//  Created by Eduardo on 2/17/14.
//  Copyright (c) 2014 Eduardo. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
