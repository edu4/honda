//
//  FrameView.h
//  HondaAR
//
//  Created by Eduardo on 2/17/14.
//  Copyright (c) 2014 Eduardo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import "MasterViewController.h"

@protocol FrameImageDelegate;

@interface FrameView : MasterViewController <UIGestureRecognizerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, UIAlertViewDelegate>
{
	IBOutlet UISegmentedControl *camerasControl;
	AVCaptureVideoDataOutput    *videoDataOutput;
	dispatch_queue_t            videoDataOutputQueue;
	AVCaptureStillImageOutput   *stillImageOutput;
	UIView                      *flashView;
	BOOL                        isUsingFrontFacingCamera;
	CGFloat                     beginGestureScale;
	CGFloat                     effectiveScale;
}

-(void)     setupAVCapture;
-(IBAction) switchCameras:(id)sender;
//-(IBAction) showFrameSelector:(id)sender;

@property(nonatomic, weak)          FrameView                   *captureManager;
@property(nonatomic, weak)          IBOutlet UIView             *previewView;
@property(nonatomic, weak)          UILabel                     *scanningLabel;

@property(nonatomic, weak)          AVCaptureSession            *captureSession;
@property(nonatomic, retain)        AVCaptureVideoPreviewLayer  *previewLayer;
@property(nonatomic, weak)IBOutlet  UIImageView                 *frameImageView;

//Delegate to load still image in PreviewController
@property(nonatomic,weak)           id  <FrameImageDelegate>    frameImageDelegate;

@end
@protocol FrameImageDelegate <NSObject>

- (void)didSelectStillImage:(NSData *)image withError:(NSError *)error;
//- (void)didSelectStillImageWithFrame:(NSData *)image withError:(NSError *)error;

@end