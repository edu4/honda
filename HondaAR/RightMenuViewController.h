//
//  RightMenuViewController.h
//  HondaAR
//
//  Created by Eduardo on 2/17/14.
//  Copyright (c) 2014 Eduardo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"


@interface RightMenuViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *mainMenuTable;

@end
