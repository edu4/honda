//
//  ARPhotoPreview.m
//  HondaAR
//
//  Created by Eduardo on 2/17/14.
//  Copyright (c) 2014 Eduardo. All rights reserved.
//

#import "ARPhotoPreview.h"

@interface ARPhotoPreview ()
@property (nonatomic) int imageIndex;
@end

@implementation ARPhotoPreview
{
    UIImage *effectImage;
    UIImage *imageBeforeEffect;
    NSMutableArray *effectArray;
}
@synthesize aRPhotoPreview;

//Load still image without frame
- (void)didSelectStillImage:(NSData *)imageData withError:(NSError *)error
{
    if(!error)
    {
        effectImage = [[UIImage alloc] initWithData:imageData];
        aRPhotoPreview.image = effectImage;
        
        GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:effectImage];
        GPUImageSepiaFilter *stillImageFilter = [[GPUImageSepiaFilter alloc] init];
        [stillImageSource addTarget:stillImageFilter];
        [stillImageSource processImage];
        effectImage = [stillImageFilter imageFromCurrentlyProcessedOutput];
        aRPhotoPreview.transform = CGAffineTransformMakeRotation(M_PI / 2);
        [aRPhotoPreview setImage:effectImage];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Capture Error" message:@"Unable to capture photo." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

}

-(void)noEffect
{
    //self.view = aRPhotoPreview;
}
-(void)sepiaEffect
{
    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:effectImage];
    GPUImageSepiaFilter *stillImageFilter = [[GPUImageSepiaFilter alloc] init];
    [stillImageSource addTarget:stillImageFilter];
    [stillImageSource processImage];
    effectImage = [stillImageFilter imageFromCurrentlyProcessedOutput];

    [aRPhotoPreview setImage:effectImage];
    aRPhotoPreview.transform = CGAffineTransformMakeRotation(M_PI / 2);
    
}

- (void)didReceiveMemoryWarning
{   
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
