//
//  ARPhotoPreview.h
//  HondaAR
//
//  Created by Eduardo on 2/17/14.
//  Copyright (c) 2014 Eduardo. All rights reserved.
//

#import "MasterViewController.h"
#import "ARCameraPreview.h"
#import "FrameView.h"
#import "GPUImage.h"

@protocol ARPhotoPreviewDelegate;

@interface ARPhotoPreview : MasterViewController <FrameImageDelegate>//, //FrameImageDelegate>

@property (nonatomic, weak) id <ARPhotoPreviewDelegate> previewControllerDelegate;
@property (nonatomic, weak) IBOutlet UIImageView        *aRPhotoPreview;



//- (void)infiniteScrollPicker:(InfiniteScrollPicker *)infiniteScrollPicker didSelectAtImage:(UIImage *);

@end
