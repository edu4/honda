//
//  SlideShowView.m
//  HondaAR
//
//  Created by Eduardo on 2/17/14.
//  Copyright (c) 2014 Eduardo. All rights reserved.
//

#import "SlideShowView.h"

@interface SlideShowView ()

@end

@implementation SlideShowView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"スライドショー作成";
    [self.view setBackgroundColor:[UIColor colorWithRed:207.0/255.0
                                                  green:16.0/255.0
                                                   blue:13.0/255.0
                                                  alpha:1.0]];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
