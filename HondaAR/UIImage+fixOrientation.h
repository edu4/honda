//
//  UIImage+fixOrientation.h
//  HondaAR
//
//  Created by Eduardo on 2/18/14.
//  Copyright (c) 2014 Eduardo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
